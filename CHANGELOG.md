# Changelog
## [2.1.0] - 2022-01-14
### Added
- MutationObserver to watch content changes to resize scrollbar.
- observerConfig prop.

### Updated
- Optimizations by code structure.
- README

## [2.0.0] - 2021-07-12
### Removed
- scrollOffsetHeight property.
- init method.

### Added
- barColor property for coloring bars without styles.
- offsettingBar instead scrollOffsetHeight. All styles calculate inside component.
- resize method contains createStyles and moveBar;

## [1.0.1] - 2020-01-06

### Added

- CHANGELOG

### Fixed

- README


## [1.0.0] - 2020-01-06

### Added

- README

